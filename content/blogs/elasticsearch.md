---
title: Elasticsearch. An Intro.
description: 'A topdown introduction to Elasticsearch.'
toc: true
authors:
  - jonathan
tags: 
    - Elasticsearch
categories: 
    - 'Software Engineering'
series:
    - Distributed Services
date: '2021-04-20T13:11:22+08:00'
lastmod: '2021-04-20T13:11:22+08:00'
featuredImage:
draft: false
---
This blog post is a topdown introduction to Elasticsearch to help you understand some underlying concepts of Elasticsearch.

<!--more-->

## 0. Elasticsearch in a nutshell

[Elasticsearch](https://www.elastic.co/elasticsearch/) is a **distributed, elastically scalable, near-real-time** search and data analysis engine. Elasticsearch not only supports full-text search, but also supports structured search, data analysis, complex language processing, geographic location and relationship between objects.

## 1. Basic concepts:

In order to walkthrough this article more smoothly, let's first quickly introduce some basic terminology of Elasticsearch. Those terms are listed in the order according to the inclusion relationship:

- **Cluster**: cluster is a group of servers (nodes) and each server (node) is running an Elasticsearch instance.
- **Elasticsearch Index**: Elasticsearch Index is where Elasticsearch stores data. It can be temporarily analog as the `database` in the relational database.
- **Type**: Type is where Elasticsearch stores its data object definition, which can be temporarily analog as the `table` in the relational database.
- **Document**: Data, JSON format.
- **Field**: Fields within the document. Most fields are defaultly all inverted indexed and only those inverted indexed fields can be searched.

## 2. Why use the Elasticsearch

- **Simple and convenient, easy to use**
  <br>Elasticsearch is essentially a application layer abstraction of [Apache Lucene](https://lucene.apache.org/). Lucene is the most advanced, high-performance, and versatile search engine library at present. But the shortcomings are also very obvious. Lucene is a **Java library** and is extremely complicated (you need an information retrieval PhD degree to fully understand it [source](https://www.elastic.co/guide/en/elasticsearch/guide/index.html)). Elasticsearch encapsulates many excellent features and distributed operations of Apache Lucene into several convenient JSON based RESTful APIs. As a developer, there is no need for me to rack my brains to integrate this Java library into my projects and manually implement distributed operations over it. We can just simply call APIs! No need to reinvent the wheel!

- **High availability and consistency achieved as a distributed system**
  - All indexes can be configured for sharding.
  - Each shard can be configured with multiple replicas.
  - Each shard (including replica) can be used to search / read / insert new data because each shard is an independent Apache Lucene instance.

- **Near-real-time searching speed**

## 3. Real examples about how to use Elasticsearch

Concepts are always abstract, let's look at some real world examples about how to use Elasticsearch.

### 3.0 Basic CRUD

Elasticsearch use HTTP Verbs (PUT, DELETE, POST, GET, HEAD) to classify different operations.

Request format: `<host>:<port>/<index>/<type>/<endpoint>? <parameters>`.

#### Upsert data (update/insert)

In the following example, we will insert some personal information of "John Smith" into the `employee` type under the the Elasticsearch Index `megacorp` (corresponding to `employee` table in the `megacorp` database in the relational database)

The purpose of `pretty` keyword at the end of the request is to jsonify the return result

```bash
curl -X PUT "localhost:9200/megacorp/employee/1?pretty" 
     -H 'Content-Type: application/json' 
     -data '
        {
            "first_name" : "John",
            "last_name" :  "Smith",
            "age" :        25,
            "about" :      "I love to go rock climbing",
            "interests": [ "sports", "music" ]
        }
        '

```

Return

```json
{
    "_index" : "megacorp",
    "_type" : "employee",
    "_id" : "1",
    "_version" : 1,
    "result" : "created",
    "_shards" : {
        "total" : 2,
        "successful" : 1,
        "failed" : 0
    },
    "_seq_no" : 0,
    "_primary_term" : 1
}

```

Let's look at the fields in the return:

- `_index`, `_type`, and `_id`: Each of them correspond to the the Elasticsearch Index, Type name, and Document ID of the inserted data.
- `_version`, `_seq_no`, and `_primary_term` : the Elasticsearch implement concurrency control through the version number mechanism of optimistic lock. The version number can be brought in the request, whose usage can be divided into two situations: asyc replication and database migration (See the official website [document](https://www.elastic.co/guide/en/elasticsearch/reference/current/optimistic-concurrency-control.html) for details).
- `_shards` : In the above example, there are 2 shards in the the Elasticsearch instance, and the data is inserted into one of the shards.

Besides, there're some other interesting point worth mentioning:

- the Elasticsearch document is immutable. That is, to update a document, the Elasticsearch will not go to the memory / disk to locate the old document, and then do an in-place update. However, the Elasticsearch will write the data of multiple documents in a segment file in the disk, whenever we want to insert / update / delete, the Elasticsearch will only append the update information in the most recently edited segment file. Therefore, in a segment file, the new and old version of the same Document coexist.
<br>The reason behind this design has something to do with the implementation of the Apache Lucene. This design may be difficult to understand at first glance. However, it is generally very inefficient to locate the old document in a file and update it in-place (searching + locking). If the document is immutable, the new document can be directly written to the new segment without worrying about the old one. There are also cache advantages. This optimization is the key reason why the Elasticsearch does high-speed search, and I will talk about it in great detail in the following section.

- If deleted, the syntax is similar. the Elasticsearch will record the `Document ID` of the document that needs to be deleted to a file dedicated to storing invalid data and then delete it asynchronously (this is so-called tombstone, which is a very common design method).

> Simple Search

In the following example, we will search all data with "rock climbing" in all `about` fields under `employee` Type of the the Elasticsearch Index of `megacorp` :

Request

```bash
curl -X GET "localhost:9200/megacorp/employee/_search?pretty" 
     -H 'Content-Type: application/json' 
     -d'
        {
            "query" : {
                "match" : {
                    "about" : "rock climbing"
                }
            }
        }
        '

```

Return

```json
{
   ...

   "hits": {
      "total":      2,
      "max_score":  0.16273327,
      "hits": [
         {
            ...
            "_score":         0.16273327, 
            "_source": {
               "first_name":  "John",
               "last_name":   "Smith",
               "age":         25,
               "about":       "I love to go rock climbing",
               "interests": [ "sports", "music" ]
            }
         },
         {
            ...
            "_score":         0.016878016, 
            "_source": {
               "first_name":  "Jane",
               "last_name":   "Smith",
               "age":         32,
               "about":       "I like to collect rock albums",
               "interests": [ "music" ]
            }
         }
      ]
   }
}

```

The return data is still very intuitive:

- `hits`: all data that hit the search criteria.
- `_score`: the Elasticsearch use well-known TF-IDF algorithm to compute the score of each search result [Source](https://lucene.apache.org/core/4%5C_0%5C_0/core/org/apache/lucene/search/similarities/TFIDFSimilarity.html). Of course, you can also define your own similarity algorithm, but this is beyond the scope of this article.
- The `about` field in the second return result only hit the "rock" in "rock climbing," so the matching score is lower than the first data. This also shows that the Elasticsearch defaults to full-text search (Full-Text Search), which will The text requested by the user is segmented before searching. If the user wants to force the search for the entire phrase (that is, character string matching), he can use the `match_phrase` syntax in Elasticsearch Query DSL.

### 3.1 Structured Search & Aggregation

In addition to the simple CRUD operations seen above, Elasticsearch also supports some relatively complex structured searches (similar to the complex conditon statement in SQL) and aggregation operations. We also quickly look at some simple examples:

> Complex Condition Search & Structured Search

In this search request, we want to search for all data under the employee type of `megacorp` `employee` type of megacorp, the the Elasticsearch Index, with the `last name`"smith" and older than `30`.

Request

```bash
curl -X GET "localhost:9200/megacorp/employee/_search?pretty" 
     -H 'Content-Type: application/json' 
     -d'
        "query" : {
            "bool" : {
                "must" : {
                    "match" : {
                        "last_name" : "smith" 
                    }
                },
                "filter" : {
                    "range" : {
                        "age" : { "gt" : 30 } 
                    }
                }
            }
        }
        '

```

> Aggregations

In this aggregation request, we want to cluster according to the `interests` field (that is, `group by` in SQL ), and calculate the average age of all data in each group (the Elasticsearch is called `bucket`), and store the result in the `avg_age` field.

Request

```bash
curl -X GET "localhost:9200/megacorp/employee/_search?pretty" 
     -H 'Content-Type: application/json' 
     -d'
        {
            "aggs" : {
                "all_interests" : {
                    "terms" : { "field" : "interests" },
                    "aggs" : {
                        "avg_age" : {
                            "avg" : { "field" : "age" }
                        }
                    }
                }
            }
        }
        '

```

Return

```json
...
"all_interests": {
   "buckets": [
      {
         "key": "music",
         "doc_count": 2,
         "avg_age": {
            "value": 28.5
         }
      },
      {
         "key": "forestry",
         "doc_count": 1,
         "avg_age": {
            "value": 35
         }
      },
      {
         "key": "sports",
         "doc_count": 1,
         "avg_age": {
            "value": 25
         }
      }
   ]
}

```

The return result is very clear and will not be repeated here.

### 3.2 Interesting small functions

In this section, let's look at some convenient and interesting small functions of Elasticsearch.

> Highlight

By adding `highlight` keyword in the search request, Elasticsearch achieve keyword highlighting in the returned results by adding `<em>` around the hit keyword.

Request:

```bash
curl -X GET "localhost:9200/megacorp/employee/_search?pretty" 
     -H 'Content-Type: application/json' 
     -d'
        {
            "query" : {
                "match_phrase" : {
                    "about" : "rock climbing"
                }
            },
            "highlight": {
                "fields" : {
                    "about" : {}
                }
            }
        }
        '

```

Return:

```json
{
   ...
   "hits": {
      "total":      1,
      "max_score":  0.23013961,
      "hits": [
         {
            ...
            "_score":         0.23013961,
            "_source": {
               "first_name":  "John",
               "last_name":   "Smith",
               "age":         25,
               "about":       "I love to go rock climbing",
               "interests": [ "sports", "music" ]
            },
            "highlight": {
               "about": [
                  "I love to go <em>rock</em> <em>climbing</em>" 
               ]
            }
         }
      ]
   }
}

```

- In addition, it also supports fuzzy query, geographic location query, etc. 

## 4. Underlying Design (core of this blog post)

This section is the core of this article. We will gradually understand how Elasticsearch achieves near-real-time search while ensuring high data availability using a topdown approach. 

Recall that I give you a brief introduction of some basic terminology of Elasticsearch in the "Section 1 Basic Concepts" by making some analogy with terminologies in relational database. We will start this section by first redefining those basic concepts more rigorously.

### 4.1 Cluster

- Definition: A cluster is composed of one or more nodes, and an Elasticsearch instance is running on each node. Elasticsearch's cluster, as its name says, comes with good elasticity, which will automatically handle load balancing, and does not require users to manually add/remove nodes when load is changed.
- Just like any distributed system, in order to ensure the consistency of data between each server and the consensus of internal state, Elasticsearch also has the definition of master node.
    - The only responsibility of the master node is to manage cluster-level operations, such as creating and deleting an the Elasticsearch index, or adding / deleting a node in the cluster (the responsibility is similar to the relationship between server and agent in `Consul`), and the master node does not participate in any document-level operation. Each node can be a master. When the master node service is down due to unexpected accidents, the leader election will be carried out through the Paxos algorithm. The Paxos algorithm is a little too complicated, and I will write another blog post to make an analysis and comparsion between the Paxos algorithm and the Raft protocols.
    - Because each node is an Elasticsearch instance, the user can send a request to any node, and the requested node will perform service discovery, locate the node where the target data is stored, forward the request, and process the data back to the user. This specific process will be described below in detail. Note that the master node does not participate in the above operations.

### 4.2 the Elasticsearch Index

- Definition: Index is the place where the Elasticsearch stores data. In section 1, I said its role is similar to the `database` in the relational database. However, the Elasticsearch Index is actually a logic namespace of multiple physical shards. Each shard is an instance of Apache Lucene (i.e. an independent search engine), and the user's documents will be stored on the partitions of each shard, but the user will not directly interact with the partition. Instead, the the Elasticsearch index is responsible for the interaction and load balancing of users data among shards and partitions.
- The relationship here may be a bit messy, I drew a picture here to sort it out for everyone:
![/blogs/elasticsearch/image/1611887977528_cd2052449c69156ab6ed2a80ba4f6ad0.png](/blogs/elasticsearch/image/1611887977528_cd2052449c69156ab6ed2a80ba4f6ad0.png)

  + All the documents (JSON data) are stored in physical shards.
  + Each shard is governed by only one node.
  + Each node can store multiple shards.
  + The user directly sends the request to any node. The node that accepts the request is responsible for service discovery and forwarding the request to the node that processes the data
  + The shard is divided into two types: the **primary shard** and **the replica**. The number of primary shards is fixed when creating the index, but the number of replica shards owned by each primary shard can be dynamically configured.
  + <span style="color:red"> [Q] </span> You may have questions here. Since the request is directly sent to any node, how do you determine which shard stores the data?

    <span style="color:green"> [A] </span> Elasticsearch uses a very common database rebalancing design pattern: `hash % N`:

    Each Document written to the Elasticsearch will have a unique Document ID, which may be included in the user requests or automatically generated by Elasticsearch if the user doesn't specify the ID. When writing or querying the data according to Doucment ID, the Elasticsearch will use following formula to locate which one of shards should this document.

    `shard # = hash(document_id) % number_of_primary_shards`

    Therefore, just ensure that the hash function is uniform [ [what is uniform](https://en.wikipedia.org/wiki/SUHA_%5C(computer_science%5C)) ], the load of each shard is balanced, which is why the number of primary shards can only be defined when creating the index, and cannot be changed later.

- One of the main goals of Elasticsearch when it was originally designed was flexible horizontal scalability. Let's look at an example of Elasticsearch's scalability:
    - Index has three main shards (P0, P1, P2). Each main shard has a copy shard (R0, R1, R2)
    ![/blogs/elasticsearch/image/1611887977340_1a39c66a60f8fa1c559b52508b69882a.png](/blogs/elasticsearch/image/1611887977340_1a39c66a60f8fa1c559b52508b69882a.png)

    - After adding a node (the Elasticsearch is not as convenient as the Consul, adding a new node requires manual configuration of IP and other information in the config file)
    ![/blogs/elasticsearch/image/1611887977335_992048f3c2545fcb6f3b9312b28ea5b2.png](/blogs/elasticsearch/image/1611887977335_992048f3c2545fcb6f3b9312b28ea5b2.png)

    - Or dynamically increase the number of replica shards from 1 to 2. The Elasticsearch will ensure that each node will not have duplicate shards to guarantee the availbility of the service even if some nodes are down.
    ![/blogs/elasticsearch/image/1611887977438_8a0662bbdd25bd9dcd105da8ea575cdd.png](/blogs/elasticsearch/image/1611887977438_8a0662bbdd25bd9dcd105da8ea575cdd.png)

- <span style="color:red"> [Q] </span> How the Elasticsearch performs service discovery when a new node is added?
    <br><span style="color:green"> [A] </span> the Elasticsearch will read configuration file, perform service discovery using the classic Gossip Procotol to check the health status of each node. Requests sent by the elasticsearch support unicast and multicast.

- <span style="color:red"> [Q] </span> How does the Elasticsearch redistribute shards in nodes to protect data availability and achieve load balancing after adding / and deleting a node?
    <br><span style="color:yellow"> [The author asked] </span> This is actually a very simple algorithm problem, see if you can use greedy algorithms to come up a solution by yourself?

- <span style="color:red"> [Q] </span> How do primary shards interact with replica shards?
    <br><span style="color:green"> [A] </span> Let's first look at the basic CRUD:
    <br>The general idea is: **first update primiary shard, synchronously update replica**.
    ![/blogs/elasticsearch/image/1611887977356_135608a2eccd5f512eaa7bf881f746e3.png](/blogs/elasticsearch/image/1611887977356_135608a2eccd5f512eaa7bf881f746e3.png)

    1. User sends the CRUD request to node 1.
    2. Node 1 calculates the hash of the document ID and finds that the document should be stored on shard 0, and the main shard of shard 0 is currently stored on node 3.
    3. Node 1 forwards the request to node 3, node 3 performs relevant data operations on the document.
    4. Each update of the data will be written into the primary shards first, and then synchronized to the replica shards. If the node 3 is updated successfully, node 3 will forward the update request to node 1 and node 2 in parallel. After node 1 and node 2 are updated, node 3 returns the request execution result to the user.

    Note:

    - <span style="color:red"> [Q] </span> Can we perform the replication asynchronously? 
    <br><span style="color:green"> [A] </span> This is a classic trade-off in system design, but the Elasticsearch team do not recommend asynchronous replication. On the one hand, it's difficult to guarantee data consistency. On the other hand, the Elasticsearch can not achieve **Back Pressure** when performing asynchronous replication. If there is a request requires very long execution time and occupies lots of resources, return the result early may cause the user to be too optimistic to the server's capacity, and sent a bunch of new requests which results in overload the server. With back pressure on, the server will return `HTTP 503` to the user when the sever is overloaded to stop the user from sending more requests.
    - As with all distributed systems, data consistency granularity is configurable:
        - **One**: Return to the user as long as one of the nodes succeeds [Not recommended].
        - **All**: All nodes are successful before returning.
        - **Most**: The result is returned to the user after `quorum` number of nodes success.
            <br>Really classic formula in the distributed system:
            <br>`quorum = int( (primary + number_of_replicas) / 2 ) + 1`


    Read Document:
    ![/blogs/elasticsearch/image/1611887977418_48e639f064d486db8336b6599b9c8b9f.png](/blogs/elasticsearch/image/1611887977418_48e639f064d486db8336b6599b9c8b9f.png)

    - Because we ensure data consistency when writing, either the primary shard or the replica shard can be used to read data, the Elasticsearch uses Round-Robin to balance the request load.
    - If asynchronous writing is configured before, the primary shard may have the newest data but the replica shard might not be updated, resulting in inconsistent return results.


### 4.3 Search
In this blog post, we will talk about Elasticsearch's incredible searching ability in four blocks: **Mapping**, **Analysis**, **Query DSL**, and **near-real-time search speed**.

*Analysis*: Mainly responsible for preprocessing the inserted data including word segmentation (Tokenization), name entity recognition (NER), and weight calculation (TF-IDF), etc. Because it mainly involves the knowledge of the NLP field, I will skip it here.

*Query DSL:* Just some syntax of searching query in the Elasticsearch. Please look at the Elasticsearch's official documentation on the website. I also skipped here.

*Mapping:* The Elasticsearch needs to impose schema constraints on the inserted data, and the mapping file stores the schema that defines the data format type. Mapping file's syntax is similar to the JSON Schema in many NoSQL Database. Elasticsearch also needs such a configuration file to determine How to build an inverted index of a field.

When we insert a document, Elasticsearch's semantic analyzer will automatically generate a mapping file for us according to the field's content. The following is an example of a mapping file:

```json
{
    "gb": {
        "mappings": {
             "tweet": {
                "properties": {
                   "date": {
                      "type": "date",
                      "format": "dateOptionalTime"
                   },
                   "name": {
                       "type": "string"
                   },
                   "tweet": {
                      "type": "string"
                   },
                   "user_id": {
                      "type": "long"
                   }
                } 
            }
        } 
    }
}

```

In fact, automatically generated mapping file will have a lot of pitfalls.
It's generally recommended to define our own mapping file by hand.

In this blog post, I focus more on discussing how the Elasticsearch can achieve **close to real-time search speed** using a distributed architecture.

### How does the Elasticsearch achieve near-real-time distributed search? (Distributed Search Execution)

Difficulties:
* The matching result of any search request is unknown, so every search must query all the data in every shards.
* Search matching results are diverse, and there may be different mapping between documents. 
* It is also necessary to analyze the matched result and compute scores/weights and sort search results based on the score, as well as pagination.

The solution proposed by the Elasticsearch: 
<br>divided the search process into two major stages: **Query-then-fetch**

* Query phase

  Searching in a distributed architecture is essentially selecting the Top-K data with the largest score from multiple data nodes (a frequently asked interview question LMAO). Therefore, the general idea of the Elasticsearch is that broadcast the user's request to every shard (which may be primary or replica), execute the query on each shard, calculate the matching score, and build a priority queue to filter out the Top-K results.
  ![/blogs/elasticsearch/image/1611887977427_6b4c78d96d96936be214cdb1e20370c6.png](/blogs/elasticsearch/image/1611887977427_6b4c78d96d96936be214cdb1e20370c6.png)

    1. The user sends a query to the node 3, and the node 3 will first establish an empty priority queue. The priority queue reservation space size depends on the pagination parameter, if there is no pagination parameter, the Elasticsearch will set it to `1000` by default
    2. Node 3 forwards the query to all shards of the other two nodes through round-robin (including replica shards), which is why the more replicas can increase the throughput of the search. We can let node 1 do the searches on first 1000 documents on the primary shard of shard 1, and do the searches on the 1000-2000 documents on the replica shard of shard 1 on node 2 (like multi-threaded processing searches).
    3. Each shard executes a query locally, stores the matched document ID on a local prioirty queue at each node ranked by document score, and returns the result to node 3.
    4. Node 3 sorts all document IDs returned by all the other nodes on a global priority queue and return the selected Top-K results to the user.

    If you want a query to search multiple Elasticsearch indexes, the process is exactly the same as the above. Do you remember the definition of the Elasticsearch Index? Each the Elasticsearch Index is essentially an application layer logic namespace pointing to physical shards on nodes. 

* Fetch phase

    In the Query phase, we got all the document IDs. In fetch phrase, we grab the data of the document through these IDs, do some post-data processing, and return it to the user.
    ![/blogs/elasticsearch/image/1611887977291_48a4261512fd50a3bde69b2c060e7a72.png](/blogs/elasticsearch/image/1611887977291_48a4261512fd50a3bde69b2c060e7a72.png)

    1. Node 3 broadcast `GET` requests to other shards using round-robin based on document IDs.
    2. Calculate the hash according to document IDs to locate the shard and node storing that shard. Once located, the post processing steps (such as the keyword highlighting mentioned earlier) will be performed. The document data will be returned to node 3 after completion.
    3. After all requests are returned, node 3 will integrate the results (pagination, etc.) and return them to the user

In this small section, I walk though how the Elasticsearch use a Query-thread-Fetch pattern to achieve absolute load balance for each search request to each server, and maximize the concurrency of search processing by broadcasting requests to all primary and replica shards. 

**Note:** Because the request is broadcasted using round robin, if the score of two documents is exactly the same, and we sort documents solely according to the score, it is likely to cause the order of those two documents returned differently by Elasticsearch for sending the exactly same request multiple times. In order to solve this problem, the Elasticsearch provides a `preference` parameter (generally configured as the user's current web session ID). After the first search, the Elasticsearch will store the request order of the each shard and some search parameters to the cache, with the `preference` parameter as the key. In this way, we ensure that future searches will traverse all the shards in the exact same order of as the first request as long as `preference` value hasn't been changed.

### Let's dive a little bit deeper. Why can it be **near-real-time**?

  In fact, Elasticsearch have way more optimization to achieve the near-real-time searching speed. In this section, I will dive a little bit deeper to the underlying achitecture. 

  Recall that in Section 3 I said the the Elasticsearch Document is Immutable?

  >Elasticsearch's document is immutable. That is, to update a document, the Elasticsearch will not go to the memory/disk to locate the old document, and then do an in-place update. However, the Elasticsearch will write the data of multiple documents in a segment file in the disk, whenever we want to insert/update/delete, the Elasticsearch will only append the update information in the most recently edited segment file. Therefore, in a segment file, the new and old version of the same Document coexist.
  <br>The reason behind this design has something to do with the implementation of the Apache Lucene. This design may be difficult to understand at first glance. However, it is generally very inefficient to locate the old document in a file and update it in-place (searching + locking). If the document is immutable, the new document can be directly written to the new segment without worrying about the old one. There are also cache advantages. This optimization is the key reason why the Elasticsearch does high-speed search, and I will talk about it in great detail in the following section.


  This is actually a very classic data storage data structure: **LSM Tree** (Log-structured Merge Tree) with **SSTable** (Sorted String Table). Many famous data storage system, such as Google's LevelDB and BigTable, Facebook's Cassandra, and the well-known SQLite, use LSM-Tree as underlying data structure, instead of the B+Tree which we often learned in school.

  Here, I will briefly introduce the LSM Tree, if you want more in-depth understanding of LSM, I strongly recommend the book "Design Data Intensive Application" or my personal reading note of this book[blog](https://www.notion.so/Design-Data-Intensive-Application-73dcecac532f4d94bd1082aa56c5b9a7). Those resources cover a comprehensive introduction to LSM Tree and a detailed comparsion between B-Tree with LSM Tree. If possible, I strongly recommend the Google's Bigtable paper [paper](https://research.google/pubs/pub27898/). This paper not only proposes the groundbreaking optimization of data storage, but also has a profound impact on the column-oriented database in many OLAP today.

  SSTable structure diagram:
  ![/blogs/elasticsearch/image/1611887977358_7403f4910fb221024b9ec275048ff10a.png](/blogs/elasticsearch/image/1611887977358_7403f4910fb221024b9ec275048ff10a.png)

  The overall structure can be divided into a **Memtable** in memory, which stores Key-Value pairs, and lots of **Segment files** on disk that store the actual data.
  - **Memtable**: Key is string and will be sorted according to the key. Value is a pointer pointing to the segment file with a byte offset pointing to the exact data location on the disk. Elasticsearch will first do word segmentation for each input document, and then establish inverted index for each word. Therefore, for the Elasticsearch, the key in memory stores the inverted words. Value stores an array of pointers to the segment files (secondary index), and finally each secondary index points to the actual offset on the disk.
  - **Segment file**: the Elasticsearch will perform word segmentation on each input document, and then create an inverted index for each word, so what is stored in the segment file is the posting list of all document IDs where the indexed word exists.

  In the LSM Tree, the writing and updating of data are append-only to a segment file. When the segment file reaches a certain size, the file will automatically split into a new segment files. That is, if I insert/update a document to the Elasticsearch, the Elasticsearch will append that data(i.e. posting list) to the last updated segment file. The disk offset and file pointer of this segment file are written to the memtable in memory, instead of reading the memtable to locate the data and then do inplace updates. Elasticsearch will periodically merge the segment files on disk through a background process to ensure that segment files on disk will not occupy too many system resources(i.e. file descriptors). This design has the following benefits:

  - **Append-only = sequential write operation (sequential writing).**
  Sequential writes are fast!!!! Sequential writes are fast!!!! Sequential writes are fast!!!! The cache advantage of sequential writes is very obvious, and it performs well on both HDD and SSD. If we do inplace updates instead, because user requests are random, it is easy to cause random disk reads and writes. As we all know, the more random disk reads and writes the architecture has to suffer, the worse a storage system design is.
  - **There is no need to worry about concurrency control and crash recovery.** Because the reading and writing of data are append-only, there is no need to lock at all. And we don't need to worry about the system crashs when we overwrite a data, which causes the new data and old data coexist in the database. Elasticsearch will simply write a special identifier immediately after successfully writing the data, and the data without the special identifier at the end will be skipped.
  - **Merge segment files periodically in the background can greatly avoid the gradual fragmentation of disk space. (Defragmentation)** Even if the merging process sometimes consumes too much CPU's I/O resources, Elasticsearch provides throttle API for developers to have finer control of the merging process.
  - **MemTable in memory also does not need to create a Key for each string, because the Key is sorted. We only need to save the last string of the segment file when the file reaches a certain size.** For example, suppose we have a MemTable like following:

      ```
      {
          ...
          "adapt":  segment 1,  offset  0,
          "admire":  segment 2,   offset  31,
          ...
      }
      ```

      Suppse we want to query `administration`. We can first lookup `administration` using binary search, and find that `administration` is between `adapt` and `admire`. So we only need to query the data from `segment 1, offset 0` to `segment 2, offset 31`. This optimization ensures that Memtable can always be in memory.

### Efficently update the data
  As mentioned above, adding a new document requires rebuilding the inverted index. How can the newly built inverted index be optimized to maximize the advantage of immutability of segment file in Elasticsearch?

  The solution is Elasticsearch uses Lucene's **Pre-Segment Search** method. The general idea is that when a new document is to be inserted, the entire inverted index will not be rebuilt, and an In-memory buffer will be written first. When the data in the buffer stays for a certain period of time, the documents in the buffer will be built into a new inverted index (subsidiary index), and written (commit) in a new index file (segment). When searching, each index will be requested, and the final result will be integrated and returned to the user.

  Here, let's go through this process in detail:

  1. When there is a new document to be inserted, the entire index will not be rebuilt. First, each file will be built with an inverted index, and then it will be stored in an In-memory buffer. In the future, reading and writing in a short period of time will give priority to querying the index in memory. If there is no hit, then drop the disk query.

      ![/blogs/elasticsearch/image/1611887977399_59f866034a614fba68a96594bce15c12.png](/blogs/elasticsearch/image/1611887977399_59f866034a614fba68a96594bce15c12.png)

  2. When a certain time limit is exceeded (default 1s, dynamically configurable), the Elasticsearch will commit the document in the buffer. Commit will store the information metadata of the current document in a commit log file, which is mainly used to replay the previous data when the system crashes to ensure the durability of the data.
  3. After leaving a record in the Commit Log, the inverted index of the document in the buffer built in the previous step will be written to the new Segment file (subsidiary index). This process becomes refresh (refresh)
  4. Write the + known Segment file path of this new Segment file path to a new Commit Point file and write it to disk .🤫🤫🤫🤫🤫🤫🤫🤫 ( pay attention to this small expression)
  5. Execute the fsync system call and write all the data in the cache to disk .🤫🤫🤫🤫🤫🤫🤫🤫 ( pay attention to this small expression)

      ![/blogs/elasticsearch/image/1611887977294_4fd2042f5b96d215aa916fcfcd04640a.png](/blogs/elasticsearch/image/1611887977294_4fd2042f5b96d215aa916fcfcd04640a.png)

  6. Mark the new Segment as searchable. In the future, the search request will traverse the Segment files marked as searchable on all disks to get the hit Posting List. Through the two data structures of the commonly used Skip List and the the Elasticsearch self-developed [Roaring Bitmap](https://www.elastic.co/blog/frame-of-reference-and-roaring-bitmaps), all the hit Posting List will be made a nd and get the final Document IDs
  7. Mark the in-memory buffer as empty to accept new document
- Deletes and Updates

  Segment is Immutable, so neither deletion nor update changes the old Segment file.

  In addition to the Commit Point file, the Elasticsearch will also maintain a .del file. When a document is deleted, the offset XX in Segment 1 will be recorded in the .del file, which is also known as tombstone. When searching, the Posting List marked for deletion in each segment will still be searched, but it will be removed when the result is finally returned to other nodes

  Update is also a similar principle, the old document will be marked as deleted, the new version of the document is written to the new Segment, the new and old versions will be searched when searching, and filtered when returning.

  Through the above introduction, we understand that the Elasticsearch optimizes the writing speed by using SSTable + LSM Tree to force disk sequential writing, and caches inverted indexes to memory and SSTable binary lookup to optimize the reading speed, and the Elasticsearch formulates default processes to regularly merge disk Segment files to optimize resource occupancy. However, the real core is not here yet.

- I don't think there is anything special, but it feels complicated, why is it Near Real-Time Search?

  Do you remember that I asked everyone to remember this little expression? In fact, these steps marked by the little expression were not executed by the Elasticsearch at all. I lied to everyone

  > 将这个新Segment文件路径的 + 已知Segment文件路径写到一个新 Commit Point 文件, 写入磁盘. 🤫🤫🤫🤫🤫🤫🤫🤫 (注意这个小表情)

    > 执行fsync系统调用, 将所有缓存中的数据写入磁盘. 🤫🤫🤫🤫🤫🤫🤫🤫 (注意这个小表情)

    The above-mentioned method of establishing a search according to the Immutable Segment file greatly improves the document update efficiency, but it is still not fast enough. The real bottleneck is the **disk**. For even if we use the LSM Tree. The real bottleneck is the fsync system call, fsync **is very, very** time-consuming. The idea of the Elasticsearch is to directly remove fsync, and you don't have to wait for the Segment file to persist before starting the search. At the same time, the Elasticsearch also makes full use of Lucene support when the file is written to the filesystem cache. When marked as searchable,

    ![/blogs/elasticsearch/image/1611887977499_cbd3c3f94a446559bc3a8e940981bec6.png](/blogs/elasticsearch/image/1611887977499_cbd3c3f94a446559bc3a8e940981bec6.png)

- Making Changes Persistent

    Removing fsync seems absurd, so how can data be persisted?

    But in fact the Elasticsearch has its own method, that is **translog**. In fact, every change to the Elasticsearch will be written into the translog.

    Let's take a look at the actual insertion process of the Elasticsearch:

    1. When a document is successfully indexed, we not only write to the In-memory buffer, but also write a data to the Translog

    ![/blogs/elasticsearch/image/1611887977395_ddd26b7f47e33fc0bd99b967f0dea5e6.png](/blogs/elasticsearch/image/1611887977395_ddd26b7f47e33fc0bd99b967f0dea5e6.png)

    1. Refresh once per second: the document in the in-memory-buffer is written to a new Segment, the file is synchronized to disk **without calling** fsync, the new Segment is directly marked as searchable, and stored in the filesystem cache
    2. Mark in-memory buffer as empty, new Segment file as searchable

    ![/blogs/elasticsearch/image/1611887977488_d3926d3ade6b754acbb9d0b72022e6af.png](/blogs/elasticsearch/image/1611887977488_d3926d3ade6b754acbb9d0b72022e6af.png)

    1. After several iterations, more and more files in the translog

    ![/blogs/elasticsearch/image/1611887977329_8c415dfa8b4f5d7fc59b1c580a3a63e5.png](/blogs/elasticsearch/image/1611887977329_8c415dfa8b4f5d7fc59b1c580a3a63e5.png)

    1. When the file in the translog exceeds a certain size or a certain time limit (or the default is 30mins, which can be dynamically configured), execute a full commit
        - Writes all existing files in the current buffer to the new Segment file
        - Mark in-memory-buffer as empty
        - Write new Commit Point to disk, ⚠ ️ Only this time will drop the disk Commit Point
        - Empty file system cache, call fsync
        - Mark Translog as empty

    ![/blogs/elasticsearch/image/1611887977305_2a1a577839119920c8fb2adb43460f36.png](/blogs/elasticsearch/image/1611887977305_2a1a577839119920c8fb2adb43460f36.png)

    Every time the Elasticsearch exits the instance, it will force the disk to drop a Translog. Every time the Elasticsearch starts, it will check the latest Commit Point, read the Segment file pointed to by the Commit Point, and read these files from the disk to the cache, and play back each operation in the Translog. In this way, the Elasticsearch will return to the state before exiting the instance.

    Translog itself will default to synchronizing fsync with the disk every 5 seconds. You may think, what if the node hangs up in the 5-second interval, remember that each shard actually has multiple replicas and is isolated from each other Does it affect each other? The possibility of the main fragment and the copy fragment hanging up at the same time is very low, and there is always a fragment of data that is complete. Of course, you can also configure the fsync synchronization evaluation rate, but it can sacrifice certain performance.

At this point, we have learned that the Elasticsearch minimizes the call to `fsync` by Translog and uses filesystem cache to achieve amazing query speed. In addition, the Elasticsearch uses the mechanism of regularly placing files in Translog to quickly search for inserts while ensuring the persistence of data. Achieve true **near real-time** search and **high availability** of data

## 5. Conclusion

The author has not been in contact with Elasticsearch before writing this document, but the author found himself getting started with relevant documents and books quite easy and easy. On the one hand, because the official documents of Elasticsearch are concise and clear, on the other hand, the author has used Facebook's Cassandra before, and the design of the two underlying layers is extremely similar. The underlying layer also uses LSM-Tree to optimize, and distributed design has many similarities. The author's biggest feeling is that most system designs **are actually unchangeable**. Daily reading accumulation may not be immediate for the moment. Income, but it is still beneficial to your long-term personal technology growth.

## 6. Extended reading:

- The Father of Google Search Key Papers [The Anatomy of a Large-Scale Hypertextual Web Search Engine](http://infolab.stanford.edu/%5C~backrub/google.html)
- Cassandra thesis [Cassandra - A Decentralized Structured Storage System](https://www.cs.cornell.edu/projects/ladis2009/papers/lakshman-ladis2009.pdf)
- Bigtable Paper (Father of LSM Tree) [Bigtable: A Distributed Storage System for Structured Data](https://static.googleusercontent.com/media/research.google.com/en//archive/bigtable-osdi06.pdf)
- [Lucene in Action](https://www.manning.com/books/lucene-in-action)
- [Frame of Reference and Roaring Bitmaps](https://www.elastic.co/blog/frame-of-reference-and-roaring-bitmaps)
- [Analysis of Lucene - Basic Concepts](https://www.alibabacloud.com/blog/analysis-of-lucene---basic-concepts%5C_594672)
- Amazon abandons the Elasticsearch and reuses native Lucene [What Amazon gets by giving back to Apache Lucene](https://aws.amazon.com/blogs/opensource/amazon-giving-back-apache-lucene/)
- [In-depth understanding of Elasticsearch writing process](https://zhuanlan.zhihu.com/p/94915597)
- [Lucene query principle and analysis](https://www.infoq.cn/article/ejeg02vroegvalw4j_ll)
- [Elasticsearch The Definitive Guide](https://www.elastic.co/guide/en/elasticsearch/guide/index.html)
- Tencent on the Elasticsearch kernel bottom optimization blog post [Tencent Elasticsearch massive scale behind the kernel optimization analysis](https://zhuanlan.zhihu.com/p/139725905)