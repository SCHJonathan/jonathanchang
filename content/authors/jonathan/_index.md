---
title: Jonathan Chang
role: Senior year undergraduate student
avatar: images/avatar.jpg
organization:
  name: University of Illinois at Urbana Champaign
  url: https://cs.illinois.edu/about
bio: Stay safe and wear mask :D
social:
  - icon: envelope
    iconPack: fas
    url: mailto:jonathanyuheng@gmail.com
  - icon: twitter
    iconPack: fab
    url: https://twitter.com/schjonathanc
  - icon: github
    iconPack: fab
    url: https://github.com/SCHJonathan
---

## Self Introduction

Hello folks! My name is Yuheng (Jonathan) Chang from Taiyuan, Shanxi, China. I'm currently a senior year computer science student at the [University of Illinois at Urbana Champaign](https://cs.illinois.edu/about) and expected to be graduated in May, 2022. I really enjoy being a software engineer especially building large-scale distributed services, but I also love every other part of my life. Frankly, I have so many wishes waiting to be achieved. Some might just sound unreasonable, but I have the confidence to make all of them come true, which keeps me busy and happy.

## Fun Facts

- **King of the Junk**
<br>I got this nickname purely due to the fact that I was the major customer for nearly all of the fast-food restaurants near my college. I'm too impatient to cook myself (but I actually am a talented cook) and too poor (PepeCry) to enjoy a decent meal. With tons of fast-food consumption experience, I finally made the conclusion: Within the scope of fast food restaurants, the Popeye has the best chicken and the Burger King's double whopper is the best burger. Change my mind!

- **Fake Love**:
<br>I freaking love music. I spend at least 2 hours every day with music but I have never been to a concert. I freaking love traveling. I have been to lots of major cities in China and spent 16 days in Singapore but the farthest place I have ever been to in the United States is Chicago, which is literally 4-hour-driving away from my university. Well, at least "Travel around the U.S." and "Go to Joji and Drake's concert" are on my wish list now.

- **Competitive Programming**
<br>Forget when, where, and how. Suddenly I feel amused by joining programming competitions. However, the fact is my algorithm isn't good and I am sucked at math. I screencasted every coding competition I have joined. Don't worry. I will never publicize the recording unless I win that competition (which probably won't happen in the forseenable future).
