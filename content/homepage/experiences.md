---
title: Experiences
draft: true
experiences:
  - title: Software Engineer Intern
    organization:
      name: Apple
      url: https://www.apple.com/
    dates: 'May 2021 - August 2021'
    location: Shanghai, China
    writeup: >
      
  - title: Software Engineer Intern
    organization:
      name: ByteDance Inc.
      url: https://www.bytedance.com/en/
    dates: 'Octobor 2020 - March 2021'
    location: Beijing, China
    writeup: '**Skills**: Golang, Python, Apache Kafka, Elasticsearch, MySQL (InnoDB), Flask, Docker, Jenkins, Redis, Consul'

  - title: Software Engineer Intern
    organization:
      name: Alibaba Group
      url: https://www.bytedance.com/en/
    dates: 'May 2020 - August 2020'
    location: Hangzhou, China
    writeup: '**Skills**: Java, SpringBoot, Memcached'

weight: 2
widget:
  handler: experiences

  # Options: sm, md, lg and xl. Default is md.
  width: lg

  sidebar:
    # Options: left and right. Leave blank to hide.
    position: left
    # Options: sm, md, lg and xl. Default is md.
    scale:
  
  background:
    # Options: primary, secondary, tertiary or any valid color value. Default is primary.
    color:
    image:
    # Options: auto, cover and contain. Default is auto.
    size:
    # Options: center, top, right, bottom, left.
    position:
    # Options: fixed, local, scroll.
    attachment: 
---